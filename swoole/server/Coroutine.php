<?php
/**
 * Swoole 文档 Coroutine 对象
 * @author huimingdeng <1458575181@qq.com>
 * @version swoole-4.3.0
 * @link https://wiki.swoole.com/wiki/page/p-coroutine.html Coroutine
 */
namespace Swoole\Server;
/**
 * Class Coroutine
 * @package Swoole\Server
 */
class Coroutine {}