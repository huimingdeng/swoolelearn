# Swoole 介绍
使 PHP 开发人员可以编写高性能的异步并发 TCP、UDP、Unix Socket、HTTP，WebSocket 服务。Swoole 可以广泛应用于互联网、移动通信、企业软件、云计算、网络游戏、物联网（IOT）、车联网、智能家居等领域。 使用 PHP + Swoole 作为网络通信框架，可以使企业 IT 研发团队的效率大大提升，更加专注于开发创新产品。

同类型引擎介绍： `WorkMan` 、`easy-swoole`、`swoft` 等。

## swoole 安装与学习准备
一键安装或编译安装，初步学习(新手)使用一键安装方式，深入了解可以编译安装。

当前学习环境 `swoole4.3.0` + `php7.2.15` + `CentOS7.2`/`Ubuntu16.4` . `Ubuntu16.4` : 一键安装方式安装的 `swoole4.3.0-alpha` 版，`CentOS7.2` 新手入门级编译安装 `swoole.4.3.0-stabled` .

`PHPstorm` 安装 `swoole` 代码提示工具。 `Github` 搜索 `swoole ide-helper` 安装。 `File` -> `Settings` -> `Languages & Frameworks` -> `PHP` -> `Include Path` 中添加 `ide-helper` 存放路径，保存设置。

### 函数
创建服务端 `Swoole\Server(string $host, int $port = 0, int $mode = SWOOLE_PROCESS,
    int $sock_type = SWOOLE_SOCK_TCP)`

创建异步非阻塞客户端 `Swoole\Client(SWOOLE_SOCK_TCP, SWOOLE_ASYNC)`
