<?php
/**
 * Created by PhpStorm.
 * User: huimingdeng
 * Date: 2019/3/26
 * Time: 23:30
 */

$client = new Swoole\Client(SWOOLE_SOCK_TCP, SWOOLE_SOCK_ASYNC);

// 绑定连接事件
$client->on('connect', function(swoole_client $cli){
	// $cli -> send("GET / HTTP/1.1\r\n\r\n");
});

// 异步回调客户端，4.3： 目前不做效果 recv 
$client->on("receive", function(swoole_client $cli, $data){
	echo "Receive: $data".PHP_EOL;
});

// 错误信息输出
$client->on("error", function(swoole_client $cli){
	echo "error".PHP_EOL;
});

// 绑定关闭事件
$client->on("close", function (swoole_client $cli)
{
	echo "Connection close".PHP_EOL;
});

$client->connect('127.0.0.1', 9800) || exit("");
// 长连接测试
swoole_timer_tick(10000, function () use($client)
{
	$client->send(rand(1,10));
});
