<?php
/**
 * Swoole 文档 Client 对象
 * @author huimingdeng
 * @version swoole-4.3.0
 * @link https://wiki.swoole.com/wiki/page/p-client.html Client
 */
namespace Swoole\Server;

/**
 * Class Client
 * @package Swoole\Server
 */
class Client {
	/**
	 * 当connect/send/recv/close失败时，会自动设置$swoole_client->errCode的值。
	 * errCode的值等于Linux errno。
	 * @var integer
	 * @link https://wiki.swoole.com/wiki/page/172.html 附录：Linux的errno定义
	 */
	public $errCode;

	/**
	 * sock属性是此socket的文件描述符
	 * @var integer
	 */
	public $sock;
	/**
	 * 表示此连接是新创建的还是复用已存在的。与SWOOLE_KEEP配合使用
	 * @var boolean
	 * @since 1.8.0
	 */
	public $reuse;

	/**
	 * Client 构造函数
	 * @param integer $sock_type socket类型 TCP/UDP
	 * @param integer $is_sync 表示同步阻塞还是异步非阻塞，默认为同步阻塞
	 * @param string $key 用于长连接的Key，默认使用IP:PORT作为key。相同key的连接会被复用
	 */
	public function __construct(int $sock_type, int $is_sync = SWOOLE_SOCK_SYNC, string $key) {}
	/**
	 * 设置客户端参数，必须在connect前执行。
	 * @param array $settings
	 * @since 1.7.17
	 * @link https://wiki.swoole.com/wiki/page/p-client_setting.html Client - 配置选项
	 */
	public function set(array $settings) {}

	/**
	 * 注册异步事件回调函数。 同步阻塞客户端一定不要使用on方法
	 * @param  string $event    事件类型，支持connect/error/receive/close 4种
	 * @param  mixed  $callback 回调函数，可以是函数名字符串、匿名函数、类静态方法、对象方法
	 * @return integer
	 */
	public function on(string $event, mixed $callback) {}

	/**
	 * 连接到远程服务器
	 * @param  string      $host    远程服务器的地址
	 * @param  int         $port    远程服务器端口
	 * @param  float       $timeout 网络IO的超时，包括connect/send/recv，
	 *                              单位是s，支持浮点数。默认为0.5s，即500ms
	 * @param  int|integer $flag    在UDP类型时表示是否启用udp_connect
	 *                              设定此选项后将绑定$host与$port，
	 *                              此UDP将会丢弃非指定host/port的数据包。
	 *                              在TCP类型，$flag=1表示设置为非阻塞socket，
	 *                              connect会立即返回。
	 *                              如果将$flag设置为1，那么在send/recv
	 *                              前必须使用swoole_client_select来检测是否完成了连接
	 * @return bool                远程服务器的地址
	 * @since 1.10.0 或更高版本已支持自动异步解析域名
	 */
	public function connect(string $host, int $port, float $timeout = 0.5, int $flag = 0) {}

	/**
	 * 返回swoole_client的连接状态
	 * @return boolean false，表示当前未连接到服务器 true，表示当前已连接到服务器
	 */
	public function isConnected() {}

	/**
	 * 此方法可以得到底层的socket句柄，返回的对象为sockets资源句柄
	 * @return resource sockets
	 */
	public function getSocket() {}

	/**
	 * 获取客户端socket的本地host:port
	 * @return array 调用成功返回一个数组，如：array('host' => '127.0.0.1', 'port' => 53652)
	 */
	public function getSockName() {}

	/**
	 * 获取对端socket的IP地址和端口，仅支持SWOOLE_SOCK_UDP/SWOOLE_SOCK_UDP6类型的swoole_client对象
	 * 此函数必须在$client->recv() 之后调用
	 * @return boolean
	 */
	public function getPeerName() {}

	/**
	 * 获取服务器端证书信息;必须在SSL握手完成后才可以调用此方法;
	 * 可以使用openssl扩展提供的openssl_x509_parse函数解析证书的信息
	 * @return string|bool 执行成功返回一个X509证书字符串信息 执行失败返回false
	 * @since 1.8.8
	 * 需要在编译swoole时启用 --enable-openssl
	 */
	public function getPeerCert() {}

	/**
	 * 发送数据到远程服务器，必须在建立连接后，才可向Server发送数据
	 * @param  string $data 参数为字符串，支持二进制数据
	 * @return bool       成功发送返回的已发数据长度 失败返回false，并设置 $swoole_client->errCode
	 * @link https://wiki.swoole.com/wiki/page/372.html socket缓存满处理
	 */
	public function send(string $data) {}

	/**
	 * 向任意IP:PORT的主机发送UDP数据包
	 * 仅支持SWOOLE_SOCK_UDP/SWOOLE_SOCK_UDP6类型 Client
	 * @param  string $ip   目标主机的IP地址，支持IPv4/IPv6
	 * @param  int    $port 目标主机端口
	 * @param  string $data 要发送的数据内容，不得超过64K
	 * @return bool
	 */
	public function sendto(string $ip, int $port, string $data) {}

	/**
	 * 发送文件到服务器，本函数是基于sendfile操作系统调用实现
	 * @param  string      $filename 指定要发送文件的路径
	 * @param  int|integer $offset   上传文件的偏移量，可以指定从文件的中间部分开始传输数据;
	 *                               可用于支持断点续传
	 * @param  int|integer $length   发送数据的尺寸，默认为整个文件的尺寸
	 * @return bool                如果传入的文件不存在，将返回false;执行成功返回true
	 * @since 1.7.5+
	 */
	public function sendfile(string $filename, int $offset = 0, int $length = 0) {}

	/**
	 * 用于从服务器端接收数据, 1.7.22-:bool $waitall
	 * @param  int|integer  $size    接收数据的缓存区最大长度，此参数不要设置过大，
	 *                               否则会占用较大内存
	 * @param  bool|integer $waitall 是否等待所有数据到达后返回
	 * @return bool                 成功收到数据返回字符串
	 *         						连接关闭返回空字符串
	 *                				失败返回 false，并设置$client->errCode属性
	 * @since 1.7.22+
	 */
	public function recv(int $size = 65535, int $waitall = 0) {}

	/**
	 * 关闭连接
	 * @param  bool|boolean $force 第一个参数设置为true表示强制关闭连接，
	 *                             可用于关闭SWOOLE_KEEP长连接
	 * @return bool|boolean        操作成功返回 true
	 *                             当一个swoole_client连接被close后不要再次发起connect。
	 *                             正确的做法是销毁当前的swoole_client，
	 *                             重新创建一个swoole_client并发起新的连接
	 */
	public function close(bool $force = false) {}

	/**
	 * 调用此方法会从事件循环中移除当前socket的可读监听，停止接收数据
	 * 此方法仅停止从socket中接收数据，但不会移除可写事件，所以不会影响发送队列
	 * sleep操作与wakeup作用相反，使用wakeup方法可以重新监听可读事件
	 * @since 1.7.21+
	 */
	public function sleep() {}

	/**
	 * 调用此方法会重新监听可读事件，将socket连接从睡眠中唤醒
	 * 如果socket并未进入sleep模式，wakeup操作没有任何作用
	 * @since 1.7.21+
	 */
	public function wakeup() {}

	/**
	 * 动态开启SSL隧道加密
	 * 客户端创建时类型必须为非SSL
	 * 客户端已与服务器建立了连接
	 * 可同时用于同步和异步客户端，异步客户端需要传入一个回调函数
	 * @param  callback $func [description]
	 * @return [type]        [description]
	 */
	public function enableSSL(callback $func = null) {}
}