23节--swoole基础课（结课）

复习22节-- 数据库连接池（MySQL/Redis Pool）

定时清除空闲连接。（设置检测时长）

public function init(){
	$this->chanel=new \Swoole\Coroutine\Channel($this->maxConnection); // $this->maxConnection 最大连接数 >= 1*n n>0
	... ...
}

function Coroutine\Channel->push(mixed $data, float $timeout = -1) : bool;
向通道中写入数据。

判断当前通道是否为空，函数原型：
public function isEmpty(): bool


定时回收空闲连接：

public function gc(){
	swoole_timer_tick(2000,function(){ // 测试 2s 验证
		$list = [];
		while(true){
			if(!$this->channel->isEmpty() && $this->channel->length()>$this->minConnection){
				... ...
				// 当前最后一次
				if((time()-$obj['last_used_time'])>$this->idleTiem){
					$this->currentConnection--; // 当前连接数减少
				}else{
					array_push($list,$obj);
				}
			}else{
				break;
			}
		}
		foreach($list as $obj){
			$this->chanel->push($obj);
		}
	});
	
}