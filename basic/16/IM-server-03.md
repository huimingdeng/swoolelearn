## IM-SERVER 03 小案例 ##
客户端掉线如何处理？ 心跳设置，断线重连？

### 实现 ###
关闭事件触发：

	class CloseListner{
		public function handle($event){
			// 删除当前客户端所在redis 当中的路由信息
			// ip => 
			$request = WebSocketContext::get($event['fd'])['request'];
			$token = $request->header['sec-websocket-protocol'];
			$key = 'ming123456';
			$arr = JWT::decode($token,$key,['HS256']);
			$userInfo = $arr->data;
			$key = $userInfo->service_url;
			$uid = $userInfo->uid;
			$redis = $event['redis'];
			$redis->hdel($key,$uid);
			//var_dump($request);
			// var_dump("关闭事件触发");
		}
	}

`WebSocket.php`	中 `close` 代码：

	public function close($server,$id){
		$path = WebSocketContext::get($id); // close 事件无法获取，则用上下文获取token等信息
		Event::trigger('ws.close', ['server'=>$server]);
	}

`WebSocketContext.php`	代码：

	public static function init($fd,$path,$request){
		//...
	}

### 广播与私聊 ###
消息推送系统，在线则连接服务器，同送消息等。 客户端和服务端连接可能不一样的服务器。因此要实现不同服务器的客户端交互:

	C1 \			/ S1	C2->S1

	C2 ->  IM-SERVER -> S2	C1->S2

	C3 /			\ S3	C3->S3

P.S. C2->S1 : 表示 客户端1 连接的 IM-SERVER 为 服务器1的IM-SERVER -- route 作为中转，告诉服务端要发送广播消息

`MessageLinstener.php`
	
	/**
	 * @Listener(ws.message)
	 */
	class MessageListener{
		public function handle($event){
			$frame = $event['frame'];
			$redis = $event['redis'];
			$server = $event['server'];
			$data = jseon_decode($frame->data,true);
			switch($data['method']){
				case 'server_broadcast':
					$request = WebSocketContext::get($frame->fd)['request'];
					$token = $request->header['sec-websocket-protocol'];
					$key = 'ming123456';
					$arr = JWT::decode($token,$key,['HS256']); // 要引入JWT
					$userInfo = $arr->data;
					$key = $userInfo->service_url;
					// 不在则通过路由广播消息(排除当前机器)
					$service = $redis->SMEMBERS('im_service');
					// var_dump($service);
					foreach($service as $k=>$v){ //当前匹配则排除
						$url_data = json_decode($v,true);
						if($url_data['ip'].':'.$url_data['port']==$key){
							unset($service[$k]);
						}
					}
					//var_dump('过滤后:'.$service);
					$data = ['method'=>'route_broadcast', 'target_server'=>]; //UDP 传送消息|Websocket 传送
					// 如果当前服务连接了客户端那么直接发送
					$this->sendAll($server, $data['msg']);
					break;
				case 'route_broadcast':
					$server_url = $data['target_server'];
					foreach($server_url as $k=>$v){
						$push_data = json_decode($v, true);
					}
					break;
			}
		}
		// 该方法应在服务端封装 WebSocket，发送消息
		public function sendAll($server, $msg){
			foreach($server->connections as $fd){
				// var_dump($fd);
				if($server->exists($fd)){
					$server->push($fd, $msg);
				}
			}
		}
	}

P.S. `WebSocket.php` 的 message 要触发事件 `Event::trigger('ws.message')`	

