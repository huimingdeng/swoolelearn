<?php
/**
 * Created by PhpStorm.
 * User: huimingdeng
 * Date: 2019/3/27
 * Time: 20:45
 */

class Route
{
    protected $redis; // 安装 redis 客户端 编译安装 和服务端 yum install redis
    protected $server;

    public function run(){
        //$this->server->on('open');
        echo 'test';
    }

    public function workerStart($server, $worker_id){
        $this->redis = new Redis();
        // $this->redis->setOption(); //设置超时时间，设置 socket 超时时间
        $this->redis->pconnect('127.0.0.1',6378);
        // redis 连接池保护服务，超过则舍弃等
    }

    public function open(){

    }

    public function request($request, $response){
    	
    }

    /**
     * @param $server
     * @param $frame
     */
    public function message($server, $frame){
        $data = json_encode($frame->data,true);
        $fd = $frame->fd;
        switch ($data['method']) {
            case 'register':
                // 假设使用 Redis Set，命令操作 或 使用 hash
                // 组合 redis 的 key
                $service_key = 'service:'. $data['serviceName'];
                //              --------
                //                 |______ 自定义前缀
                echo $service_key;
                $value = [
                    'ip' => $data['ip'],
                    'port' => $data['port']
                ];
                $this->redis->sadd($service_key, json_encode($value));
                $redis = $this->redis;
                // 会存在宕机的情况，主动清除
                $server->tick(3000, function($id, $redis, $server, $service_key, $fd) { // 非一次性定时器应该多少时长清除？业务需求设置
                    // 先检测 fd 是否存活  -- IM server 端的存活状态 --掉线删除
                    if (!$server->exist($fd)){
                        // 删除 redis 当中的服务信息
                        // var_dump('IM 宕机了');
                        $redis->del($service_key); //SREM
                        // 清除定时器
                        $server->clearTimer($id);
                    }
                }, $redis, $server, $service_key, $fd); 

                break;
            
            default:
                # code...
                break;
        }
    }

    public function close($server, $fd){

    }
}