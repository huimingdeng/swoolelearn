<?php 
/**
 * Created by PhpStorm.
 * User: huimingdeng
 * Date: 2019/4/3
 * Time: 22:14
 */
// 监听所有IP，创建服务对象
$server = new Swoole\Server("0.0.0.0",9800);

// 设置 swoole 服务
$server->set([
	'worker_num' => 1,
	'heartbeat_idle_time'=>10,//连接最大的空闲时间
    'heartbeat_check_interval'=>3 //服务器定时检查
]);

// 监听连接事件
$server->on('connect', function (swoole_server $server, $fd)
{
	echo "new connect {$fd}".PHP_EOL;
});

// 监听接收客户端发送的消息
$server->on('receive', function (swoole_server $server, int $fd, int $reactor_id, string $data)
{
	echo "client {$fd} send message: {$data}".PHP_EOL;
});

// 关闭消息
$server->on('close', function ()
{
	echo "close ".PHP_EOL;
});

$server->start();