## IM 通信 ##
架构：
	

### Route 消息路由服务

服务状态，按照算法计算出客户端连接到哪台IM-server，返回客户端，然后客户端连接服务端，保存客户与IM-server的关系

IM-server 宕机，自定从 Redis 中删除

IM-server 上线后连接到 Route，自动加入 Redis


客户端/服务端 常驻服务？

#### websocket
事件类，专门处理，分配执行操作事件。

Listener 中构建 `StartListener` 类，`OpenListener` 在框架核心中约定. 同时在框架核心创建 `Event` 目录，构造 `Event` 类

创建 `StartListener` 类：
	
	class StartListener{
		public function handle(){
			// 创建协程环境
			go(function(){
				// ...
			});		
			// 连接事件触发
			$ret = $cli -> upgrade("/");
			if($ret){
				// Config::get()
				$data = [
					'method' => 'register',
					'serviceName' => ,
					'ip' => '192.168.1.128',
					'port' => 9800
				];
				$cli->push('hello');
				// 心跳处理 swoole --- websocket 数据帧类型 -- 预定义常量 9
				swoole_timer_tick(3000,function() use($cli){ // 时间长，测试可设置短看效果
					if($cli){ //Coroutine 协程属性列表参考

					}
					$cli->push("test1-1"); // 想发送消息但不触发 message 消息，ping 包可处理
				}); //    ----  =======
				//          |_____||___  握手失败，则不断提示 Waring push 失败
				//                \|______  规范发送的消息
			}
		}
	}
创建 `OpenListener` 类：

	/**
	 * @Linstener(ws.open) 规范和约定常量名称写死固定，防止命名混乱等
	 */
	class OpenListener{
		public function handle(){}
	}


创建 `Event` 类：(目前类函数的事件都是不规范的)

	class Event{
		public static $events = [];
		/**
		 * 事件注册
		 * @param $event 事件名
		 * @param $callback 事件回调
		 */
		public static function register($event, $callback){
			$event = strtolower($event);//不区分大小写
			// 什么时候启动？
			if(!isset(self::$events[$event])){
				// 事件不存在，执行
				self::$events[$event] = [];
			}
			// 重新赋值
			self::$events[$event] = ['callback'=>$callback];
		}

		/**
		 * 事件触发 trigger ,事件管理器（eventManage）调用 trigger
		 * 触发一般是循环判断是否存在事件
		 * @param $event
		 * @param $params 参数
		 */
		public static function trigger($event,$params=[]){
			$event = strtolower($event);
			if(isset(self::$events[$event])){
				// 事件存在，则执行回调 callback，如果多个事件，则考虑优先级关系
				if(call_user_func(self::$events[$event]['callback'], $params)){
					return true;
				}
			}
			return false;
		}
	}

继承的 `Http` 类测试收集事件： (P.S. 框架知识)

	public function collectEvent(){
		$files = glob(EVENT_PATH."/*.php"); // EVENT_PATH 在 config 中定义
		if(!empty($files)){
			foreach($fiels as $dir => $file){
				include $file;
				$filename = explode('/', $file);
				$classname = explode('.',end($filename))[0];
				$nameSpace = 'App\\Listener\\'.$classname;
				//            ---------------
				//                  |__________ 可以定义替换，而不是写死固定的
				if(class_exists($nameSpace)){
					$obj = new $nameSpace;
					$re = new \ReflectionClass($obj);
					$str = $re->getDocComment();
					if(strlen($str)<2){
						throw new Exception("没有按照规则定义事件名称");
					}else{
						// 解耦
						perg_match("/@Listener\((.*)\)/i",$str,$eventName);
						if(empty($eventName)){ // ---> ①
							throw new Exception("没有按照规则定义");
						}
						Event::register('start',[$obj, 'handle']);
						//               -----
						//                 |______ 不能够固定事件名称，这样耦合度过高，--类常量
					}
					
				}
			}
		}
	}
	... ... 
		

反射类中可读取文档注释等 `ReflectionClass::getDocComment` 必须要以 `/**` 开始，则可以读取文档注释。 根据注释正则匹配，验证抛出异常①	(P.S. 要学习反射类知识)


#### 权限认证
握手的事件回调，`swoole` 官网案例：

	public function handshake(\swoole_http_request $request, \swoole_http_...){
		
	}


P.S. 集群服务，不严谨的分布式集群服务