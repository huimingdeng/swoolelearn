<?php
/**
 * Created by PhpStorm.
 * User: huimingdeng
 * Date: 2019/3/24
 * Time: 17:47
 */
$ip = "123.207.74.118";//"192.168.1.128";
$client = new Swoole\Client(SWOOLE_SOCK_TCP, SWOOLE_ASYNC);
$client->connect($ip,9800) || exit("connection failure");
$client->send("I'm a client.");
echo $client->recv(); //接收消息
// 关闭客户端连接
$client->close();//初识，接收消息后就关闭了，这是不正确的