<?php
/**
 * Created by PhpStorm.
 * User: huimingdeng
 * Date: 2019/3/24
 * Time: 17:55
 */
//tcp 协议
$server = new Swoole\Server("0.0.0.0", 9800);
//进程设置
$server -> set([
    'work_num' => 2,
]);
/**
 * 设置监听连接事件
 */
$server->on("connect", function ($server,$fd){
    echo "新连接:{$fd}".PHP_EOL;
});

$server->on("receive", function (swoole_server $server, int $fd, int $reactor_id, string $data){
    echo "消息发送过来:".$fd.PHP_EOL;
    $server->send($fd,'我是服务端');
});

$server->on('close',function (){
    echo "消息关闭".PHP_EOL;
});
//服务器开启
$server->start();

// 多端口监听案例
/*$serv = new Swoole\Http\Server("127.0.0.1", 9501);
$port2 = $serv->listen('127.0.0.1', 9502, SWOOLE_SOCK_TCP);
$port2->on('receive', function (Swoole\Server $serv, $fd, $reactor_id, $data) {
    echo "[#".$serv->worker_id."]\tClient[$fd]: $data\n";
});*/