<?php
/**
 * Swoole 文档 Server 对象
 * @author huimingdeng
 * @version swoole-4.3.0
 * @link https://wiki.swoole.com/wiki/page/14.html Server
 */
namespace Swoole\Server;

/**
 * Class Server
 * @package Swoole\Server
 */
class Server
{
    /**
     * 运行时参数数组
     * @var array
     */
    public $setting = [];
    /**
     * 当前服务器主进程的 PID
     * @var integer
     */
    protected $master_pid;
    /**
     * 当前服务器管理进程的 PID
     * @var integer
     */
    protected $manager_pid;
    /**
     * 当前Worker进程的编号
     * @var integer
     */
    public $work_id;
    /**
     * 当前Worker进程的操作系统进程ID;与posix_getpid()的返回值相同
     * @var integer
     */
    public $work_pid;
    /**
     * true 表示当前的进程是Task工作进程
     * false 表示当前的进程是Worker进程
     * @var bool
     */
    public static $taskworker;
    /**
     * TCP连接迭代器，可以使用foreach遍历服务器当前所有的连接，
     * 此属性的功能与Server->getClientList是一致的，但是更加友好。
     * 遍历的元素为单个连接的fd。
     * @var iterator
     */
    public static $connections;
    /**
     * 监听端口数组
     * @var array
     */
    public $ports;
    /**
     * Swoole Server 对象构造函数
     * @param string  $host      监听的IP地址
     * @param integer $port      监听端口
     * @param integer  $mode      运行的模式
     * @param integer  $sock_type SOCKET 类型
     * @todo information string
     */
    public function __construct(string $host, int $port = 0, int $mode = SWOOLE_PROCESS,
        int $sock_type = SWOOLE_SOCK_TCP) {
        // TODO:
    }
    /**
     * 设置运行时的各项参数
     * @param array $setting 键值队数组
     */
    public function set(array $setting)
    {
        // TODO:
    }
    /**
     * 注册Server的事件回调函数。
     * @param  string $event    [description]
     * @param  mixed  $callback [description]
     * @return [type]           [description]
     * @todo
     */
    public function on(string $event, mixed $callback)
    {
        //  TODO:
    }
    /**
     * 增加监听的端口
     * @param string $host [description]
     * @param int    $port [description]
     * @param [type] $type SOCKET类型
     */
    public function addListener(string $host, int $port, $type = SWOOLE_SOCK_TCP)
    {
        // TODO:
    }
    /**
     * 添加一个用户自定义的工作进程
     * @param Process $process 用户工作进程
     */
    public function addProcess(Process $process)
    {
        // TODO:
    }
    /**
     * 监听新服务(Server)的端口
     * @param  string $host [description]
     * @param  int    $port [description]
     * @param  int    $type [description]
     * @return [type]       [description]
     */
    public function listen(string $host, int $port, int $type)
    {
        // TODO:
    }
    /**
     * 配置好Server后启动服务的指令
     * @return bool true|false
     * @todo 启动 swoole 服务
     */
    public function start()
    {
        // TODO:
    }
    /**
     * 重新加载服务的Task进程
     * @param  bool|boolean $only_reload_taskworkrer 是否仅重启Task进程
     * @return bool        true|false
     * @link https://wiki.swoole.com/wiki/page/p-server/reload.html Server-roload
     */
    public function reload(bool $only_reload_taskworkrer = false)
    {
        // TODO:
    }
    /**
     * 使用此函数代替exit/die结束Worker进程的生命周期;
     * 使当前Worker进程停止运行，并立即触发onWorkerStop回调函数。
     * @param  int|integer  $worker_id 工作进程标识id
     * @param  bool|boolean $waitEvent 默认为false表示立即退出，
     *                                 设置为true表示等待事件循环为空时再退出
     * @return [type]                  [description]
     */
    public function stop(int $worker_id = -1, bool $waitEvent = false)
    {
        // TODO:
    }
    /**
     * 关闭服务器
     * @return [type] [description]
     * @todo kill -15 主进程PID
     */
    public function shutdown()
    {
        // TODO:
    }
    /**
     * 添加tick定时器，可以自定义回调函数。
     * @param int $time 定时时间
     * @return [type] [description]
     * @link https://wiki.swoole.com/wiki/page/414.html Server->tick
     */
    public function tick($time)
    {
        // TODO:
    }
}
