# swoolelearn

### 介绍
swoole 学习代码。

#### 01 swoole 初识

`Swoole`的绝大部分功能只能用于`cli`命令行环境，请首先准备好`Linux Shell`环境。可使用`vim`、`emacs`、`phpstorm`或其他编辑器编写代码，并在命令行中通过下列指令执行程序。

    php /path/to/your_file.php

### Swoft
介绍见 [Swoft](https://www.swoft.org/docs/ "Swoft")    

笔记见 github 仓库中的 [Swoft](https://github.com/huimingdeng/hello-world/tree/master/Note/swoole "Swoft").xmind