<!DOCTYPE HTML>
<html>
	<head>
		<title>README</title>
		<meta charset="utf-8">
		<style type="text/css">
			.container{ width:1200px; margin: 10px auto; }
			code{ color: red; padding:2px; margin-left: 5px; background: #ccc; border-radius: 2px; }
			code::before{margin-left:5px; content: "\20"; display: inline-block;}
			code::after{margin-right: 5px; content: "\20"; display: inline-block;}
			a{text-decoration: none;}
			a:hover{text-decoration: underline;}
		</style>
		<script type="text/javascript">
			/**
			 * a 节点属性设置
			 */
			function aNodeSet(){
				var alist = document.getElementsByTagName('a');
				for (var i = alist.length - 1; i >= 0; i--) {
					alist[i].setAttribute('target','blank')	;
					// console.log(alist[i].getAttribute('href'));
				}
			}
			window.onload = function(){
				aNodeSet();
			}
		</script>
	</head>
	<body>
		<div class="container">
			<?php
			require('HyperDown'.DIRECTORY_SEPARATOR.'parser.php');
			$parser = new HyperDown\Parser;
			$text = file_get_contents('README.md');
			$html = $parser->makeHtml($text);
			echo $html."\n";?>
		</div>
	</body>
</html>