<?php
/**
 * Created by PhpStorm.
 * User: huimingdeng
 * Date: 2019/3/29
 * Time: 21:33
 */

class Round{
	static $lastIndex = 0;

	public static function select(array $list){
		$currentIndex = self::$lastIndex; // 当前Index
		// 判断返回索引
		$url = $list[$currentIndex];
		if($currentIndex+1>count($list)-1){
			self::$lastIndex = 0;
		}else{
			self::$lastIndex++;
		}
		return $url; // 返回当前 url
	}
}